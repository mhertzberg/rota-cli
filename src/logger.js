/* eslint-disable */
import React, { Component } from 'react'
import { exec } from 'child_process'
class Logger extends Component {
  constructor(props) {
    super(props)
    this.stylesheet = {
      bordered: {
        border: {
          type: 'line'
        },
        style: {
          border: {
            fg: 'red'
          },
          scrollbar: {
            bg: 'blue'
          },
          hover: {
            bg: 'blue'
          }
        },
        scrollable: true
      }
    }
    this.state = {
      data: []
    }
  }

  componentWillMount() {
    return exec('npm run start', function (error, stdout, stderr) {
      this.setState({ data: stdout.split('\n') })
    })
  }

  render() {
    return (
      <box
        label="Logger (node)"
        class={this.stylesheet.bordered}
        width="100%"
        height="50%"
        scrollable
        draggable
      >
        <list
          items={[
            'apples',
            'oranges'
          ]}
        />
      </box>
    )
  }
}
