/* eslint-disable */
import React, { Component } from 'react'
import SSH from 'simple-ssh'
import blessed from 'blessed'
import assert from 'assert'
import { typeov } from 'typeov'
import { render } from 'react-blessed'
import memwatch from 'memwatch-next'

memwatch.on('leak', console.log.bind(console))

class App extends Component {
  constructor(props) {
    super(props)
  }
  render() {
    return (
      <element>
        <MDS />
      </element>
    )
  }
}

class MDS extends Component {
  constructor(props) {
    super(props)
    this.stylesheet = {
      bordered: {
        border: {
          type: 'line'
        },
        style: {
          border: {
            fg: 'red'
          }
        },
        scrollable: true
      }
    }
    this.state = {
      timeout: "5",
      machines: []
    }
    this.machines = [
      'mds-black-1',
      'mds-black-2',
      'mds-black-3',
      'mds-black-4',
      'mds-black-5',
      'mds1-blue',
      'mds2-blue',
      'mds3-blue',
      'mds4-blue',
      'mds5-blue',
      'mds1-dat1',
      'mds2-dat1',
      'mds3-dat1',
      'mds4-dat1',
      'mds5-dat1',
    ]
    this.interval = setInterval(a => {
      this.setState({
        timeout: this.state.timeout === "0" ? String(5) : String(+this.state.timeout - 1)
      })
      if (this.state.timeout === "0") {
        this.ssh.end()
        this.ssh = new SSH({
          host: 'devrc',
          user: 'ec2-user',
          agent: process.env.SSH_AUTH_SOCK,
          agentForward: true
        })
        this.connectToMachines(this.machines)
      }
    }, 10e3)
    this.ssh = new SSH({
      host: 'devrc',
      user: 'ec2-user',
      agent: process.env.SSH_AUTH_SOCK,
      agentForward: true
    })
    this.connectToMachines(this.machines)
  }

  componentWillUnmount() {
    clearInterval(this.interval)
  }

  connectToMachines(listOfMachines) {
    assert.equal(typeov(listOfMachines), 'array', '`listOfMachines` should be an Array')
    if (!listOfMachines.length) return this.ssh.start()
    const machine = listOfMachines.slice(0, 1)
    this.ssh.exec(`ssh ${machine} curl -s http://localhost:9200`, {
      out: stdout => {
        this.setState({
          machines: {
            ...this.state.machines,
            [machine]: JSON.parse(stdout)
          }
        })
      },
      exit: code => {
        if (code === 255) {
          this.setState({
            machines: {
              ...this.state.machines,
              [machine]: {
                name: 'N/A',
                status: 'DOWN',
                version: {
                  number: 'N/A',
                  build_hash: 'N/A',
                  build_timestamp: 'N/A'
                }
              }
            }
          })
        }
      }
    })
    return this.connectToMachines(listOfMachines.slice(1))
  }

  getMachineStatusesFromState(machines) {
    return [[
      'status',
      // 'refresh in',
      'name',
      'host',
      'version',
      'hash',
      'timestamp'
    ]]
    .concat(Object.keys(machines).map(mach => {
      const machine = machines[mach]
      return [
        `${machine.status === 200 ? 'OK' : machine.status}`,
        // `${this.state.timeout}s`,
        `${machine.name}`,
        `${mach}`,
        `${machine.version.number}`,
        `${machine.version.build_hash}`,
        `${machine.version.build_timestamp}`
      ]
    }))
    .concat([[].fill('', 0, 6)]) // hack to force the last row to show. react-blessed bug?
  }

  render() {
    const data = this.getMachineStatusesFromState(this.state.machines)
    return (
      <box
        label="MDS Servers"
        class={this.stylesheet.bordered}
        width="100%"
        height="50%"
      >
        <listtable
          data={data}
        />
      </box>
    )
  }
}

const screen = blessed.screen({
  autoPadding: true,
  smartCSR: true,
  title: 'MDS Monitor'
})

screen.key(['escape', 'q', 'C-c'], () => process.exit(0))

render(<App />, screen)
