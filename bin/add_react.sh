#!/usr/bin/env sh
#npm install --silent --save-dev eslint-plugin-react
echo "
@############################
// add to .eslintrc
{
  \"plugins\": [
    \"react\"
  ]
}
@############################
"
